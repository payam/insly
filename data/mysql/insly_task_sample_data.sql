SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
  `employee_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `birth_datetime` datetime DEFAULT NULL,
  `ssn_id` varchar(12) NOT NULL,
  `is_employee` tinyint(2) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `address` text NOT NULL,
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL COMMENT 'foreign_key',
  `update_datetime` datetime DEFAULT NULL COMMENT 'when last modified data',
  `update_by` int(11) DEFAULT NULL COMMENT 'foreign_key',
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `employees` (`employee_id`, `name`, `birth_datetime`, `ssn_id`, `is_employee`, `email`, `phone`, `address`, `created_datetime`, `created_by`, `update_datetime`, `update_by`) VALUES
(1,	'Payam Naderi',	'1983-06-14 00:00:00',	'0322358078',	1,	'naderi.payam@gmail.com',	'+989355497674',	'Yousef Abad, Tehran, Iran.',	'2018-12-06 08:03:07',	1,	NULL,	NULL);

DROP TABLE IF EXISTS `employees_i18n`;
CREATE TABLE `employees_i18n` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `locale` varchar(6) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` mediumtext,
  PRIMARY KEY (`id`),
  KEY `locale` (`locale`),
  KEY `row_id` (`foreign_key`),
  KEY `field` (`field`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

INSERT INTO `employees_i18n` (`id`, `locale`, `foreign_key`, `field`, `content`) VALUES
(1,	'en_US',	1,	'introduction',	'I am an innovative software programmer, Creator Of Poirot Php Framework.\r\nI Have Good point of view in Object Oriented Design and Analysis with extensive experience in the full lifecycle of the software design process including requirements definition, prototyping, proof of concept, testing, and maintenance. \r\n\r\nI already manage a small Web/IT team as a CTO and Lead Backend Developer.\r\n'),
(2,	'es_ES',	1,	'introduction',	'Soy un programador de software innovador, Creator Of Poirot Php Framework.\r\nTengo un buen punto de vista en Diseño y Análisis Orientado a Objetos con amplia experiencia en todo el ciclo de vida del proceso de diseño del software, incluida la definición de requisitos, creación de prototipos, prueba de concepto, prueba y mantenimiento.\r\n\r\nYa administro un pequeño equipo de Web / IT como CTO y desarrollador principal de Backend.'),
(3,	'fr_FR',	1,	'introduction',	'Je suis un programmeur de logiciels innovant, Creator Of Poirot Php Framework.\r\nJ\'ai un bon point de vue sur la conception et l\'analyse orientées objet avec une vaste expérience du cycle de vie complet du processus de conception logicielle, y compris la définition des exigences, le prototypage, la preuve de concept, les tests et la maintenance.\r\n\r\nJe gère déjà une petite équipe Web / informatique en tant que CTO et développeur principal.'),
(4,	'en_US',	1,	'experience_last',	'Lead Backend Developer @ DigiFilm\r\n\r\nI`ve Assigned to the project as a CTO and Lead Backend Developer to build and manage Team and to Move The Currently MVP To an Pretty Much Enterprise System.\r\nI’m closely working with Marketing To Achieve Company Goals.\r\n'),
(5,	'es_ES',	1,	'experience_last',	'Lead Backend Developer @ DigiFilm\r\n\r\nMe asigné al proyecto como CTO y desarrollador principal de back-end para crear y administrar el equipo y mover el MVP actual a un sistema bastante empresarial.\r\nEstoy trabajando estrechamente con Marketing To Achieve Company Goals.'),
(6,	'fr_FR',	1,	'experience_last',	'Développeur Backend Lead @ DigiFilm\r\n\r\nJ’ai été affecté au projet en tant que CTO et développeur backend pour créer et gérer l’équipe et pour déplacer le MVP actuel vers un système plutôt grand public.\r\nJe travaille étroitement avec le marketing pour atteindre les objectifs de la société.'),
(7,	'en_US',	1,	'educations',	'Tehran Conservatory Of Music\r\nComposition  — 2007- 2009\r\n\r\nKaraj Azad University\r\nBachelor of Science (BS), Computer Software Engineering  — 2002 - 2005 \r\n'),
(8,	'es_ES',	1,	'educations',	'Conservatorio de musica de teheran\r\nComposición - 2007- 2009\r\n\r\nUniversidad de Karaj Azad\r\nBachelor of Science (BS), Ingeniería de Software de Computadora - 2002 - 2005'),
(9,	'fr_FR',	1,	'educations',	'Conservatoire De Musique De Téhéran\r\nComposition - 2007-2009\r\n\r\nUniversité Karaj Azad\r\nBaccalauréat en sciences (BS), Génie logiciel informatique - 2002 - 2005');
