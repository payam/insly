PRODUCTION=
COMPOSE_FILES=docker-compose.yml

DEV_USER=root
DEV_GROUP=root

ifdef PRODUCTION
    COMPOSE_FILES+=
else
	COMPOSE_FILES+=-f docker-compose.local.yml
endif

help:
	@echo "up"
	@echo "  Create and start containers."
	@echo ""
	@echo "down"
	@echo "  Stop and remove containers, networks, images, and volumes."
	@echo ""
	@echo "status"
	@echo "  Shows the status of the current containers."
	@echo ""
	@echo "shell"
	@echo "  Starting a shell as user in web container."
	@echo ""


up:
	docker-compose -f $(COMPOSE_FILES) up -d

status:
	docker-compose -f $(COMPOSE_FILES) ps

down:
	docker-compose -f $(COMPOSE_FILES) down

shell:
	docker-compose -f $(COMPOSE_FILES) exec --user=$(DEV_USER) insly_web-server /bin/bash
