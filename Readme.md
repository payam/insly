## I have used some packages from my own framework named Poirot to Implement functionality

## How To Setup
Hi Dear Reviewer, I`m pretty sure you are using ``` linux ```, and have ``` docker ``` and ```docker compose``` installed.

If So,
- go to project root; you can see Makefile there
- just type ```make up```

Note: It's open 8000 port on your host to server http request, if it's not desirable change ``` docker-compose.local.yml ``` file.
[click here to view](http://localhost:8000)


## TASK 1 - Name

![Task #1](https://gitlab.com/payam/insly/raw/master/data/localhost8000_task1.jpg?inline=false "Task #1 Name")


## TASK 2 - Calculator

![Task #2](https://gitlab.com/payam/insly/raw/master/data/localhost8000_task2.jpg?inline=false "Task #2 Calculator")

![Task #2](https://gitlab.com/payam/insly/raw/master/data/localhost8000_task2b.jpg?inline=false "Task #2 Calculate")

![Task #2](https://gitlab.com/payam/insly/raw/master/data/localhost8000_task2c.jpg?inline=false "Task #2 Result")


## TASK 3 - Store employee data

Idea is to using [Entity Value attribute](https://en.wikipedia.org/wiki/Entity%E2%80%93attribute%E2%80%93value_model), 
as I know this type of Data Modeling is using with variety of Content Management Systems and also as a Built-in Translation 
Behaviour feature in CakePHP ORM.


![](https://cdn1.iconfinder.com/data/icons/hawcons/32/699251-icon-24-file-sql-48.png) 

Download .sql file include sample data + structure. 
[click here to download](https://gitlab.com/payam/insly/raw/master/data/mysql/insly_task_sample_data.sql?inline=false)


#### Query to retrieve information include Translation Locale

Query usually has built pragmatically and after retrieving result then must doing some clean up, this is because I named fields like ``` I18n_en_US__XXXX ```.   


```mysql
SELECT employees.name
   , I18n__introduction.content AS `I18n_en_US__introduction` 
   , I18n__educations.content AS `I18n_en_US__educations` 
   , I18n__experience.content AS `I18n_en_US__experience` 
FROM `employees` 
INNER JOIN `employees_i18n` AS `I18n__introduction` 
	ON  `I18n__introduction`.`foreign_key` = employees.employee_id
	AND `I18n__introduction`.`field`       = 'introduction'
	AND `I18n__introduction`.`locale`      = 'fr_FR'
INNER JOIN `employees_i18n` AS `I18n__educations` 
	ON  `I18n__educations`.`foreign_key` = employees.employee_id
	AND `I18n__educations`.`field`       = 'educations'
	AND `I18n__educations`.`locale`      = 'fr_FR'
INNER JOIN `employees_i18n` AS `I18n__experience` 
	ON  `I18n__experience`.`foreign_key` = employees.employee_id
	AND `I18n__experience`.`field`       = 'experience_last'
	AND `I18n__experience`.`locale`      = 'fr_FR'
WHERE 1
```


##

if there was any problem don't hesitate to contact me: naderi.payam@gmail.com
