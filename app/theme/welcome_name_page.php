<?php
/**
 * Note: this is just simple page; Represent Welcome Page
 *
 *       I`m not considering best practices in template renderer and dom documents
 *       such as base_url, assets, css design, etc...
 *
 * @var string $name
 * @var string $message
 */
?>

<!DOCTYPE html>
<html class=no-js itemscope itemtype=https://schema.org/ItemList>

<head>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>


<body>
    <main role="main">
        <div class="jumbotron">
            <div class="container">
                <h1 class="display-3">Welcome!</h1>
                <p><strong>I`m <a href="https://linkedin.com/in/payamnaderi/"><?= $name ?></a></strong></p>
                <p>and This is the insly recruiting test.</p>

                <?php // TODO reverse route ?>
                <p><a class="btn btn-primary btn-lg" href="/insurance/calc" role="button">Task #2 Calculator</a></p>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Task #1</h2>
                    <p><strong><?= $message ?></strong></p>
                    <p>
                        <?php for ($i=0; $i < strlen($name); $i++) { ?>
                            <p>
                            <?php for($j=0; $j<=$i; $j++) { ?>
                                <?= $name[$j] ?>
                            <?php } ?>
                            </p>
                        <?php } ?>
                    </p>
                </div>
            </div>

            <hr>

        </div>

    </main>

</body>