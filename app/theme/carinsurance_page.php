<?php
/**
 * Note: this is just simple page; Represent Simple Form
 *
 *       I`m not considering best practices in template renderer and dom documents
 *       such as base_url, assets, css design, etc...
 *
 * @var array $groups
 * @var array $flash_messages
 */
?>

<!DOCTYPE html>
<html class=no-js itemscope itemtype=https://schema.org/ItemList>

<head>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <style>
        .form_main {
            width: 100%;
        }
        .form_main h4 {
            font-family: roboto;
            font-size: 20px;
            font-weight: 300;
            margin-bottom: 15px;
            margin-top: 20px;
            text-transform: uppercase;
        }
        .heading {
            border-bottom: 1px solid #fcab0e;
            padding-bottom: 9px;
            position: relative;
        }
        .heading span {
            background: #9e6600 none repeat scroll 0 0;
            bottom: -2px;
            height: 3px;
            left: 0;
            position: absolute;
            width: 75px;
        }
        .form {
            border-radius: 7px;
            padding: 6px;
        }
        .txt2[type="submit"] {
            background: #242424 none repeat scroll 0 0;
            border: 1px solid #4f5c04;
            border-radius: 25px;
            color: #fff;
            font-size: 16px;
            font-style: normal;
            line-height: 35px;
            margin: 20px 0;
            padding: 0;
            text-transform: uppercase;
            width: 30%;
        }
        .txt2:hover {
            background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
            color: #5793ef;
            transition: all 0.5s ease 0s;
        }
    </style>

</head>


<body>

    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">
                <div class="form_main">
                    <h4 class="heading"><strong>Car insurance </strong> calculator <span></span></h4>

                    <?php foreach ($flash_messages as $type => $messages) { ?>
                        <?php foreach ($messages as $m) { ?>
                        <div class="alert <?= (($type == 'error') ? 'alert-danger' : 'alert-success') ?>" role="alert">
                            <?= $m['value'] ?>
                        </div>
                        <?php } ?>
                    <?php } ?>

                    <div class="form">
                        <form action="" method="post">
                            <input type="number" min="100" max="100000" required="" placeholder="Estimated value of the car" value="" name="car_value" class="form-control txt">
                            <p class="help-block">100 - 100 000 EUR</p>
                            <input type="number" min="0" max="100" required="" placeholder="Tax percentage" value="" name="tax_percent" class="form-control txt">
                            <p class="help-block">0 - 100%</p>
                            <input type="number" min="1" max="12" required="" placeholder="Number of instalments" value="" name="numb_instalment" class="form-control txt">
                            <p class="help-block">count of payments in which client wants to pay for the policy (1 – 12)</p>

                            <input type="submit" value="Calculate" name="submit" class="txt2">
                        </form>
                    </div>
                </div>
            </div
        </div>
    </div>

</body>