<?php
/**
 * Note: this is just simple page; Represent Simple Form
 *
 *       I`m not considering best practices in template renderer and dom documents
 *       such as base_url, assets, css design, etc...
 *
 * @var \Insly\Recruiting\Models\PriceObject $price
 * @var \Insly\Recruiting\Forms\CarInsuranceHydrate $insurance_data
 */
?>

<!DOCTYPE html>
<html class=no-js itemscope itemtype=https://schema.org/ItemList>

<head>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <style>
        .heading {
            border-bottom: 1px solid #fcab0e;
            padding-bottom: 9px;
            position: relative;
        }
        .heading span {
            background: #9e6600 none repeat scroll 0 0;
            bottom: -2px;
            height: 3px;
            left: 0;
            position: absolute;
            width: 75px;
        }
    </style>

</head>


<body>

    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">
                <div class="form_main">
                    <h4 class="heading"><strong>Car insurance </strong> calculator result <span></span></h4>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Policy</th>
                            <?php for ($i=1; $i<= $insurance_data->getNumbInstalment(); $i++) { ?>
                            <th scope="col">Instalment <?= $i ?></th>
                            <?php } ?>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Value</td>
                            <td><?= $price->getUnitAmount() ?></td>
                            <?php for ($i=1; $i<= $insurance_data->getNumbInstalment(); $i++) { ?>
                            <td></td>
                            <?php } ?>
                        </tr>
                        <?php $sumAdditions = []; ?>
                        <?php foreach ($price->getAdditionCustoms() as $ac) { ?>
                        <tr>
                            <td><?= $ac->getTitle() ?></td>
                            <td><?= $ac->getAmount() ?></td>
                            <?php for ($i=1; $i<= $insurance_data->getNumbInstalment(); $i++) { ?>
                                <?php $sumAdditions[$i] += $ac->calcAmountOnInstalment($i)->getValue() ?>
                            <td><?= $ac->calcAmountOnInstalment($i) ?></td>
                            <?php } ?>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td><strong>Total Cost</strong></td>
                            <td><?= $price->getTotalAmount() ?></td>
                            <?php for ($i=1; $i<= $insurance_data->getNumbInstalment(); $i++) { ?>
                                <td> **  <?= $sumAdditions[$i] ?></td>
                            <?php } ?>
                        </tr>
                        </tbody>
                    </table>

                    <h5>** This functionality implemented on view(template) side because otherwise make logic of objects complicated.</h5>

                    <hr/>
                    <a href="/insurance/calc"><b>+ Calculate new Insurance</b></a>


                </div>
            </div
        </div>
    </div>

</body>