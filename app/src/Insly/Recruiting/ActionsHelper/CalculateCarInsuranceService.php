<?php
namespace Insly\Recruiting\ActionsHelper;

use Poirot\Ioc\instance;
use Poirot\Ioc\Container\Service\aServiceContainer;

use Insly\Recruiting\InslyDefined;
use Insly\Recruiting\Models\PriceObject;
use Insly\Recruiting\Interfaces\iCarInsuranceAware;
use Insly\Recruiting\Models\AdditionPrice\TaxPriceAddition;
use Insly\Recruiting\Models\AdditionPrice\CommissionPriceAddition;
use Insly\Recruiting\Models\AdditionPrice\BasePremiumPriceAddition;


/**
 * Service Factory to Make Insurance Calculator
 *
 * - attach necessary events to manipulate calculation behaviour
 *
 */
class CalculateCarInsuranceService
    extends aServiceContainer
{
    /**
     * Create Service
     *
     * @return CalculateCarInsurance
     */
    function newService()
    {
        // whole services also accessible within factory service
        // so we can resolve any dependencies if it's needed
        /** @var CalculateCarInsurance $clIns */
        $clIns = \Poirot\Ioc\newInitIns(
            new instance(CalculateCarInsurance::class)
        );


        // it's just a simple demonstration in fullstack frameworks
        // we use usually merged config solutions
        //
        // without any listener it's always add nothing to whole price
        $this->_attachDefaultListeners($clIns);


        return $clIns;
    }


    // ..

    /**
     * @param CalculateCarInsurance $clIns
     */
    private function _attachDefaultListeners($clIns)
    {
        $clIns->event()->on(
            InslyDefined::EVENT_CALC_CAR_INSURANCE
            , function (PriceObject $price, iCarInsuranceAware $data)
            {
                ## Note:
                ##  Different Addition Prices can attach from different listeners
                #
                $price->addAdditionCustom(
                    new BasePremiumPriceAddition( ['numb_instalment' => $data->getNumbInstalment()] )
                );

                $price->addAdditionCustom(
                    new CommissionPriceAddition( ['numb_instalment' => $data->getNumbInstalment()] )
                );

                $price->addAdditionCustom(
                    new TaxPriceAddition( ['numb_instalment' => $data->getNumbInstalment()] )
                );
            }
        );
    }
}
