<?php
namespace Insly\Recruiting\ActionsHelper;

use Poirot\Events\Interfaces\Respec\iEventProvider;

use Insly\Recruiting\InslyDefined;
use Insly\Recruiting\Models\PriceObject;
use Insly\Recruiting\Interfaces\iCarInsuranceAware;
use Insly\Recruiting\Models\PriceObjectImmutable;
use Insly\Recruiting\Services\Events\InslyEventsHeap;
use Insly\Recruiting\Services\Events\TransferObject\TransferCalcInsurance;


/**
 * Calculate Car Insurance From Given Price
 *
 * note: separate insurance calculation logic
 *
 */
class CalculateCarInsurance
    implements iEventProvider
{
    /** @var InslyEventsHeap */
    protected $events;


    /**
     * Constructor.
     *
     * @param InslyEventsHeap $events @IoC /events
     */
    function __construct(InslyEventsHeap $events = null)
    {
        $this->events = $events;
    }


    /**
     * Get Price Include Insurance Additions
     *
     * @param iCarInsuranceAware $insInfo
     *
     * @return PriceObject
     */
    function __invoke(iCarInsuranceAware $insInfo)
    {
        $price = new PriceObjectImmutable;
        $price->setUnitAmount( $insInfo->getCarValue() );


        ## Trigger Calculation Listeners
        #
        /** @var PriceObjectImmutable $price */
        $price = $this->events->trigger(
            InslyDefined::EVENT_CALC_CAR_INSURANCE
            , new TransferCalcInsurance([
                'price'           => $price,
                'insurance_data'  => $insInfo,
            ])
        )->then(function($c) {
            /** @var TransferCalcInsurance $c */
            return $c->getPrice();
        });


        // return new price
        return $price;
    }


    // ..

    /**
     * Get Events
     *
     * @return InslyEventsHeap
     */
    function event()
    {
        return $this->events;
    }
}
