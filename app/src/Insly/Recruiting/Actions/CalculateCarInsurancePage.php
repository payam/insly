<?php
namespace Insly\Recruiting\Actions;

use Insly\Recruiting\ViewModel\RendererPhp;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use Poirot\Http\Psr\ServerRequestBridgeInPsr;

use Poirot\Std\Exceptions\exUnexpectedValue;

use Insly\Recruiting\InslyDefined;
use Insly\Recruiting\Models\PriceObject;
use Insly\Recruiting\Forms\CarInsuranceHydrate;
use Insly\Recruiting\Services\FlashMessage\FlashMessage;
use Insly\Recruiting\ActionsHelper\CalculateCarInsurance;


class CalculateCarInsurancePage
{
    /** @var RequestInterface|ServerRequestBridgeInPsr */
    protected $request;
    /** @var ResponseInterface */
    protected $response;

    /** @var CalculateCarInsurance */
    protected $insuranceCalculator;


    /**
     * Constructor.
     *
     * @param RequestInterface      $request  @IoC /HttpPsrRequest
     * @param ResponseInterface     $response @IoC /HttpPsrResponse
     * @param CalculateCarInsurance $insCalc  @IoC /helpers/CalculateCarInsurance
     */
    function __construct(
          RequestInterface $request
        , ResponseInterface $response
        , $insCalc
    ) {
        $this->request  = $request;
        $this->response = $response;

        $this->insuranceCalculator = $insCalc;
    }


    function __invoke()
    {
        # Hydrated/Validate Product From Http Request
        #
        $rData     = $this->request->getParsedBody();


        try {
            $hydEntity = new CarInsuranceHydrate($rData);
            $hydEntity->assertValidate();

        } catch (exUnexpectedValue $e) {

            ( new FlashMessage(InslyDefined::FLASH_INSURANCE_CAR_CALC) )
                ->error( $e->getMessage() );


            return $this->resRedirect();
        }


        ## Calculate Price
        #
        /** @var PriceObject $price */
        $price = ($this->insuranceCalculator)($hydEntity);



        // Render Output
        //
        // TODO I can prevent page refresh by storing result object (price) into session or a Cache Implementation Like redis
        //
        $renderer = new RendererPhp;
        $output   = $renderer->capture(
            INSLY_DIR_ROOT.'/../theme/carinsurance_calculate_page.php'
            , [
                'price'          => $price,
                'insurance_data' => $hydEntity,
            ]
        );

        return $output;
    }


    // ..

    protected function resRedirect()
    {
        // TODO from router service instance we can assemble route uri from name (reverse route)
        return $this->response->withHeader('Location', '/insurance/calc');
    }
}
