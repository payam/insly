<?php
namespace Insly\Recruiting\Actions;

use Insly\Recruiting\InslyDefined;
use Insly\Recruiting\ViewModel\RendererPhp;
use Insly\Recruiting\Services\FlashMessage\FlashMessage;


class CarInsurancePage
{
    /**
     * Display New Product Form To User
     *
     * // TODO CSRF Protection
     *         It's not that hard to implement; needs just a session handling
     *         I think priority of CSRF task is not that high because purpose of this application
     *         is on private network at least.
     *
     * @return string
     */
    function __invoke()
    {
        // Flash Message, Error During Save Data ...
        //
        $flashMessages = ( new FlashMessage(InslyDefined::FLASH_INSURANCE_CAR_CALC) )
            ->fetchMessages();


        // Render Output
        //
        $renderer = new RendererPhp;
        $output   = $renderer->capture(
            INSLY_DIR_ROOT.'/../theme/carinsurance_page.php'
            , [
                'flash_messages' => $flashMessages,
            ]
        );

        return $output;
    }
}
