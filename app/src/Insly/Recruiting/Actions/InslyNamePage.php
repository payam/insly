<?php
namespace Insly\Recruiting\Actions;

use Insly\Recruiting\InslyDefined;
use Insly\Recruiting\ViewModel\RendererPhp;


/**
 * - Welcome Message, Home
 * - Task #1
 *
 */
class InslyNamePage
{
    function __invoke()
    {
        /*
         * I`ve got this message out from Python command:
         *
         * >> import binascii
         * >> n = int('0b01110000...', 2)
         * >> binascii.unhexlify('%x' % n)
         */
        $message = 'print out your name with one of php loops';


        // Render Output
        //
        $renderer = new RendererPhp;
        $output   = $renderer->capture(
            INSLY_DIR_ROOT.'/../theme/welcome_name_page.php'
            , [
                'name'    => InslyDefined::MY_NAME,
                'message' => $message,
            ]
        );

        return $output;
    }
}
