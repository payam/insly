<?php
namespace Insly\Recruiting\Services;

use Poirot\Ioc\Container\Service\aServiceContainer;

use Poirot\Router\Interfaces\iRouterStack;
use Poirot\Router\RouterStack;


/**
 * Router Service, Match Current Request Against Actions
 *
 */
class RouterService
    extends aServiceContainer
{
    const ROUTE_NAME = 'main';

    /** @var string Service Name */
    protected $name = 'Router';


    /**
     * Create Router Service
     *
     * @return iRouterStack
     */
    function newService()
    {
        $routerStack = new RouterStack( self::ROUTE_NAME );
        return $routerStack;
    }
}
