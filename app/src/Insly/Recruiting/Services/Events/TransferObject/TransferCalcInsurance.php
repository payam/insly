<?php
namespace Insly\Recruiting\Services\Events\TransferObject;

use Poirot\Events\Event;

use Insly\Recruiting\Models\PriceObject;
use Insly\Recruiting\Models\PriceObjectImmutable;
use Insly\Recruiting\Interfaces\iCarInsuranceAware;


/**
 * Data Transfer Object On Calculate Car Insurance
 *
 */
class TransferCalcInsurance
    extends Event\DataCollector
{
    /** @var PriceObjectImmutable */
    protected $price;
    /** @var iCarInsuranceAware */
    protected $insuranceData;


    /** @return PriceObjectImmutable */
    function getPrice()
    {
        return $this->price;
    }

    function setPrice(PriceObject $price)
    {
        $this->price = $price;
    }

    /** @return iCarInsuranceAware */
    function getInsuranceData()
    {
        return $this->insuranceData;
    }

    function setInsuranceData(iCarInsuranceAware $insuranceData)
    {
        $this->insuranceData = $insuranceData;
    }
}
