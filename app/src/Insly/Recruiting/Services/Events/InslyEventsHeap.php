<?php
namespace Insly\Recruiting\Services\Events;

use Poirot\Events\Event;
use Poirot\Events\EventHeap;

use Insly\Recruiting\Services\Events\TransferObject\TransferCalcInsurance;



class InslyEventsHeap
    extends EventHeap
{
    // It can be any other thing, when it's retrieved back from persistence(model repository) or etc...
    // here we used a situation that accrued by user action to calculate insurance
    // but in real world having a flexible object model can really help.
    const CAR_CALCULATE_INSURANCE = 'car.insurance.calculate';


    /**
     * Initialize
     *
     */
    function __init()
    {
        // Calculate Price With Insurance Addition(s)
        //
        $this->bind( new Event(self::CAR_CALCULATE_INSURANCE, new Event\BuildEvent([
            'collector' => new TransferCalcInsurance ])) );
    }
}
