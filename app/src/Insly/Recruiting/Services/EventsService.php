<?php
namespace Insly\Recruiting\Services;

use Poirot\Events\Event\BuildEvent;
use Poirot\Events\Event\MeeterIoc;

use Poirot\Ioc\Container\Service\aServiceContainer;

use Insly\Recruiting\Services\Events\InslyEventsHeap;


class EventsService
    extends aServiceContainer
{
    const NAME = 'events';


    /**
     * Create Service
     *
     * @return InslyEventsHeap
     */
    function newService()
    {
        $events = new InslyEventsHeap;
        $builds = new BuildEvent([ 'meeter' => new MeeterIoc, ]);
        $builds->build($events);

        return $events;
    }
}
