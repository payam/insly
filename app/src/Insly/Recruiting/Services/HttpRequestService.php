<?php
namespace Insly\Recruiting\Services;

use Psr\Http\Message\RequestInterface;

use Poirot\Http\HttpRequest;
use Poirot\Http\Psr\ServerRequestBridgeInPsr;

use Poirot\Ioc\Container\Service\aServiceContainer;

use Insly\Recruiting\Services\Request\BuildHttpRequestFromPhpServer;


/**
 * Factory Http Object For Current Request
 *
 */
class HttpRequestService
    extends aServiceContainer
{
    const NAME = 'HttpPsrRequest';

    /** @var string Service Name */
    protected $name = self::NAME;


    /**
     * Create Http Request Service
     *
     * @return RequestInterface
     */
    function newService()
    {
        ## build request with php sapi attributes
        $request = new HttpRequest;

        // Build request object with current http request
        //
        $builder = new BuildHttpRequestFromPhpServer;
        $builder->build($request);

        return new ServerRequestBridgeInPsr($request);
    }
}
