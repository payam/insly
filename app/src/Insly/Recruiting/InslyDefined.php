<?php
namespace Insly\Recruiting;

use Insly\Recruiting\Services\Events\InslyEventsHeap;


class InslyDefined
{
    // Task #1, My Name
    //
    const MY_NAME = 'Payam Naderi';


    // Flash Message, To Display flash messages between pages after actions
    //
    const FLASH_INSURANCE_CAR_CALC = 'flash.car.insurance.calc';


    // Calculate Car Insurance
    //
    const EVENT_CALC_CAR_INSURANCE = InslyEventsHeap::CAR_CALCULATE_INSURANCE;
}
