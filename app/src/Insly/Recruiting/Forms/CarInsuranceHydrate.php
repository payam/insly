<?php
namespace Insly\Recruiting\Forms;

use Poirot\Std\tValidator;
use Poirot\Std\Hydrator\aHydrateEntity;
use Poirot\Std\Interfaces\Pact\ipValidator;
use Poirot\Std\Exceptions\exUnexpectedValue;

use Insly\Recruiting\Models\AmountObject;
use Insly\Recruiting\Interfaces\iCarInsuranceAware;


/**
 * Hydrate Car Insurance Data From Request/Data Objects
 *
 */
class CarInsuranceHydrate
    extends aHydrateEntity
    implements ipValidator
    , iCarInsuranceAware
{
    use tValidator;

    protected $carValue;
    protected $taxPercent;
    protected $numbInstalment;


    // Implement Validator:

    /**
     * Do Assertion Validate and Return An Array Of Errors
     *
     * @return exUnexpectedValue[]
     */
    function doAssertValidate()
    {
        $exceptions = [];

        if ( empty($this->carValue) )
            $exceptions[] = exUnexpectedValue::paramIsRequired('car_value');

        elseif ($this->getCarValue() < 100 && $this->getCarValue() > 100000)
            $exceptions[] = exUnexpectedValue::minMaxValueNotInRange('car_value', 100, 100000);

        if ($this->getTaxPercent() < 0 && $this->getCarValue() > 100)
            $exceptions[] = exUnexpectedValue::minMaxValueNotInRange('tax_percent', 0, 100);

        if ($this->getNumbInstalment() < 0 && $this->getCarValue() > 12)
            $exceptions[] = exUnexpectedValue::minMaxValueNotInRange('numb_instalment', 0, 12);


        return $exceptions;
    }


    // Setter Options (Data Sent From Client; Usually Html Form)

    function setCarValue($carValue)
    {
        $this->carValue = $carValue;
    }

    function setTaxPercent($taxPercent)
    {
        $this->taxPercent = $taxPercent;
    }

    function setNumbInstalment($numbInstalment)
    {
        $this->numbInstalment = $numbInstalment;
    }


    // Hydration Getters:

    /**
     * @inheritdoc
     */
    function getCarValue()
    {
        $value  =  (int) $this->_assertTrim($this->carValue);
        $amount = new AmountObject(['value' => $value]);

        return $amount;
    }

    /**
     * @inheritdoc
     */
    function getTaxPercent()
    {
        return (int) $this->_assertTrim($this->taxPercent);
    }

    /**
     * @inheritdoc
     */
    function getNumbInstalment()
    {
        $val = $this->_assertTrim($this->numbInstalment);

        if ( empty($val) )
            // Default Value
            return 1;

        return (int) $val;
    }


    // ..

    private function _assertTrim($value)
    {
        return trim($value);
    }
}
