<?php
namespace Insly\Recruiting\Interfaces;

use Poirot\Std\Interfaces\Pact\ipConfigurable;

use Insly\Recruiting\Models\AmountObject;


interface iPriceAddition
    extends ipConfigurable
{
    /**
     * Custom Name Of Type
     *
     * @return string
     */
    function getType();

    /**
     * Title
     *
     * @return string
     */
    function getTitle();

    /**
     * Amount
     *
     * @return AmountObject
     */
    function getAmount();
}
