<?php
namespace Insly\Recruiting\Interfaces\PriceAddition;

use Insly\Recruiting\Models\AmountObject;


interface iCustomRootAmountAware
{
    /**
     * Set Amount Of Root Unit Amount;
     * to calculate something like percent and percentage based discount/addition
     *
     * @param AmountObject $rootAmount
     *
     * @return $this
     */
    function setRootAmount(AmountObject $rootAmount);
}
