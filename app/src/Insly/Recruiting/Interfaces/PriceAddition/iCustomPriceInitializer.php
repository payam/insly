<?php
namespace Insly\Recruiting\Interfaces\PriceAddition;

use Insly\Recruiting\Models\PriceObject;


interface iCustomPriceInitializer
{
    /**
     * Initialize Price Object
     *
     * @param PriceObject $price
     *
     * @return $this
     */
    function setPrice(PriceObject $price);
}
