<?php
namespace Insly\Recruiting\Interfaces;

use Insly\Recruiting\Models\AmountObject;


/**
 * All necessary methods to Calculate Insurance For a Car
 *
 */
interface iCarInsuranceAware
{
    /**
     * Get Estimated value of the car
     *
     * note: It's usually (100 - 100 000 EUR)
     *
     * @return AmountObject
     */
    function getCarValue();

    /**
     * Get Tax percentage
     *
     * @return int
     */
    function getTaxPercent();

    /**
     * Get Number of instalments,
     *
     * count of payments in which client wants to pay for the
     * policy (1 – 12)
     *
     * @return int
     */
    function getNumbInstalment();
}
