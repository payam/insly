<?php
namespace Insly\Recruiting\Models;

use Poirot\Std\Exceptions\exImmutable;


class PriceObjectImmutable
    extends PriceObject
{
    /**
     * @inheritdoc
     * @throws exImmutable
     */
    function setUnitAmount(AmountObject $amount)
    {
        if ( $this->getUnitAmount() )
            throw new exImmutable('UnitAmount Is Immutable and Can`t Be Changed.');


        parent::setUnitAmount($amount);
        return $this;
    }


    /**
     * @inheritdoc
     * @throws exImmutable
     */
    function setTotalAmount(AmountObject $amount = null)
    {
        throw new exImmutable('Total Amount Is Immutable and Can`t Be Changed.');
    }
}
