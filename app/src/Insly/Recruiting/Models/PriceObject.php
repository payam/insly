<?php
namespace Insly\Recruiting\Models;

use Insly\Recruiting\Interfaces\PriceAddition\iCustomPriceInitializer;
use Poirot\Std\Struct\aValueObject;

use Insly\Recruiting\Interfaces\iPriceAddition;
use Insly\Recruiting\Interfaces\PriceAddition\iCustomRootAmountAware;


class PriceObject
    extends aValueObject
{
    /** @var AmountObject */
    protected $unitAmount;
    /** @var AmountObject */
    protected $totalAmount;
    /** @var iPriceAddition[] */
    protected $additionCustoms = [];

    protected $_c_totalAmount;
    protected $_c_additionCustomsTypes = [];


    /**
     * Set Unit Amount
     *
     * @param AmountObject $amount
     *
     * @return $this
     */
    function setUnitAmount(AmountObject $amount)
    {
        $this->unitAmount = $amount;
        return $this;
    }

    /**
     * Unit Amount
     *
     * @return AmountObject
     */
    function getUnitAmount()
    {
        return $this->unitAmount;
    }

    /**
     * Set Total Amount
     *
     * @param AmountObject|null $amount
     *
     * @return $this
     */
    function setTotalAmount(AmountObject $amount = null)
    {
        $this->_c_totalAmount = $amount;
        return $this;
    }

    /**
     * Calculate Total Amount With Custom Additions
     *
     * @return AmountObject
     */
    function getTotalAmount()
    {
        if ($this->_c_totalAmount)
            // Amount is exists
            return $this->_c_totalAmount;


        $amount      = new AmountObject;
        $amount->add( $this->getUnitAmount() );

        foreach ($this->getAdditionCustoms() as $ac)
            $amount->add( $ac->getAmount() );


        $this->_c_totalAmount = $amount;
        return $amount;
    }

    /**
     * Provide a Set of Addition Customs
     *
     * @param iPriceAddition[] $customs
     *
     * @return $this
     */
    function setAdditionCustoms(array $customs)
    {
        // reset additions
        $this->additionCustoms         = [];
        $this->_c_additionCustomsTypes = [];


        foreach ($customs as $c)
            $this->addAdditionCustom($c);

        return $this;
    }

    /**
     * Add Addition Custom To Price
     *
     * @param iPriceAddition $custom
     *
     * @return $this
     */
    function addAdditionCustom(iPriceAddition $custom)
    {
        $this->_c_totalAmount = null;

        $type = $custom->getType();
        $this->_c_additionCustomsTypes[$type] = true; // just to know this type is added


        $this->additionCustoms[] = $custom;
        return $this;
    }

    /**
     * List Addition Customs Added To Price
     *
     * @return iPriceAddition[]
     */
    function getAdditionCustoms()
    {
        foreach ($this->additionCustoms as $c) {
            if ($c instanceof iCustomRootAmountAware)
                $c->setRootAmount( $this->getUnitAmount() );

            elseif ($c instanceof iCustomPriceInitializer)
                $c->setPrice($this);
        }

        return $this->additionCustoms;
    }

    /**
     * Get Addition Customs Added To Price By Given Type
     *
     * @param string $type
     *
     * @return iPriceAddition|null
     */
    function getAdditionCustomByType($type)
    {
        foreach ($this->getAdditionCustoms() as $c) {
            if ( strtolower($type) == strtolower($c->getType()) )
                return $c;
        }

        return null;
    }

    /**
     * Has Any Addition Custom Fees
     *
     * @return bool
     */
    function hasCustoms()
    {
        return empty($this->additionCustoms);
    }

    /**
     * Has Any Custom Of Given Type
     *
     * @param string $type Custom type
     *
     * @return bool
     */
    function hasCustomByType($type)
    {
        return isset($this->_c_additionCustomsTypes[$type]);
    }
}
