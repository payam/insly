<?php
namespace Insly\Recruiting\Models;

use Poirot\Std\Struct\aValueObject;


class AmountObject
    extends aValueObject   // means we can travers and build object back from persist
{
    protected $value = 0;
    protected $currency = 'EUR';


    /**
     * Set Amount
     *
     * @param float|int $value
     *
     * @return $this
     */
    function setValue($value)
    {
        $this->value = (float) $value;
        return $this;
    }

    /**
     * Get Amount
     *
     * @return int|float
     */
    function getValue()
    {
        return $this->value;
    }

    /**
     * Add Value By Amount Object
     *
     * @param AmountObject $amount
     *
     * @return $this
     */
    function add(AmountObject $amount)
    {
        // TODO exchange from different currencies

        $value = $this->getValue();
        $this->setValue( $value + $amount->getValue() );

        return $this;
    }

    /**
     * Set Currency
     * https://en.wikipedia.org/wiki/ISO_4217
     *
     * @param string $currency
     *
     * @return $this
     */
    function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * Get Payment Currency
     *
     * @return string
     */
    function getCurrency()
    {
        return $this->currency;
    }


    // ..

    function __toString()
    {
        $curr = $this->_getLiteralCurrency( $this->getCurrency() );

        return $curr.' '.$this->getValue();
    }

    /**
     * Get Literal From String
     *
     * @param string $curr
     *
     * @return string
     */
    protected function _getLiteralCurrency($curr)
    {
        $r = [
            'EUR' => '€',
        ];


        return ( isset($r['EUR']) ) ? $r['EUR'] : $curr;
    }
}
