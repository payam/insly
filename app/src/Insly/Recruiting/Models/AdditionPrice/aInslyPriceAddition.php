<?php
namespace Insly\Recruiting\Models\AdditionPrice;

use Insly\Recruiting\Models\AmountObject;
use Insly\Recruiting\Interfaces\iPriceAddition;


abstract class aInslyPriceAddition
    extends aPriceAdditionObject
    implements iPriceAddition
{
    const TYPE = 'unknown';


    protected $instalment;

    protected $_amount_on_instalment = [];


    /**
     * Get Amount
     *
     * @return AmountObject
     */
    function getAmount()
    {
        $amount = new AmountObject;
        for($i=1; $i<= $this->getNumbInstalment(); $i++)
            $amount->add( $this->calcAmountOnInstalment($i) );

        return $amount;
    }

    /**
     * Get Amount For an Specific Instalment Number
     *
     * @param int $n
     *
     * @return AmountObject
     */
    abstract function calcAmountOnInstalment($n);


    /**
     * Number Of Instalment
     *
     * @return int
     */
    function getNumbInstalment()
    {
        if (! $this->instalment )
            $this->setNumbInstalment(1);


        return $this->instalment;
    }


    // Setter methods:

    /**
     * Set Number Of Instalments
     *
     * @param int $n
     *
     * @return $this
     */
    function setNumbInstalment($n)
    {
        $this->_amount_on_instalment = null; // reset

        $this->instalment = (int) $n;
        return $this;
    }
}
