<?php
namespace Insly\Recruiting\Models\AdditionPrice;

use Poirot\Std\Struct\aValueObject;

use Insly\Recruiting\Models\AmountObject;
use Insly\Recruiting\Interfaces\iPriceAddition;


abstract class aPriceAdditionObject
    extends aValueObject             // ability to traverse and convert to array, use for persistence use cases
    implements iPriceAddition
{
    const TYPE = 'unknown';

    /** @var AmountObject */
    protected $amount;


    /**
     * Get Custom Type
     *
     * @return string
     */
    function getType()
    {
        return static::TYPE;
    }

    /**
     * Title
     *
     * @return string
     */
    abstract function getTitle();

    /**
     * Get Amount
     *
     * @return AmountObject
     */
    abstract function getAmount();

}
