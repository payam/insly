<?php
namespace Insly\Recruiting\Models\AdditionPrice;

use Insly\Recruiting\Models\AmountObject;
use Insly\Recruiting\Interfaces\PriceAddition\iCustomRootAmountAware;


class BasePremiumPriceAddition
    extends aInslyPriceAddition
    implements iCustomRootAmountAware
{
    const TYPE = 'base_premium';


    /** @var AmountObject */
    protected $rootAmount;
    /** @var \DateTime */
    protected $userDatetime;


    /**
     * Title
     *
     * @return string
     */
    function getTitle()
    {
        return "Base Premium ({$this->_getPercentage()}%)";
    }

    /**
     * Get Amount For an Specific Instalment Number
     *
     * @param int $n
     *
     * @return AmountObject
     */
    function calcAmountOnInstalment($n)
    {
        $n = (int) $n;
        if ($n <=0 )
            throw new \InvalidArgumentException('Instalment can`t be less than one.');


        if ( isset($this->_amount_on_instalment[$n]) )
            // we have it, don`t calculate again
            return $this->_amount_on_instalment[$n];


        // We can add extra instalment value here if value of percentage depends on it
        // in exp. Step wise Pricing mechanism.
        $amount = new AmountObject;
        $prcent = ($this->_getPercentage() * $this->rootAmount->getValue() / 100) / $this->instalment;
        $amount->setValue($prcent);

        $this->_amount_on_instalment[$n] = $amount;
        return $amount;
    }

    /**
     * Current Local User Datetime
     *
     * @return \DateTime
     */
    function getUserDatetime()
    {
        if (! $this->userDatetime )
            $this->setUserDatetime( new \DateTime('now') );


        return $this->userDatetime;
    }


    // Setter methods:

    /**
     * Set User Datetime
     *
     * @param \DateTime $dateTime
     *
     * @return $this
     */
    function setUserDatetime(\DateTime $dateTime)
    {
        $this->userDatetime = $dateTime;
        return $this;
    }


    // Implement iCustomRootAmountAware:

    /**
     * Set Amount Of Root Unit Amount;
     * to calculate something like percent and percentage based discount
     *
     * @param AmountObject $rootAmount
     *
     * @return $this
     */
    function setRootAmount(AmountObject $rootAmount)
    {
        $this->_amount_on_instalment = null; // reset

        $this->rootAmount = $rootAmount;
        return $this;
    }


    // ..

    /**
     * Get Percentage Based On User Datetime
     *
     * note: base price of policy is 11% from entered car value,
     * except every Friday 15-20 o’clock (user time) when it is 13%
     *
     * @return int
     */
    private function _getPercentage()
    {
        $curWeekday = $this->getUserDatetime()->format('D');
        $curHour    = $this->getUserDatetime()->format('H');

        if ( $curWeekday == 'Fri' )
            if ( $curHour >= 15  && $curHour <= 20 )
                return 13;


        return 11;
    }
}
