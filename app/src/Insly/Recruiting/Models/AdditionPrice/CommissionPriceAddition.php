<?php
namespace Insly\Recruiting\Models\AdditionPrice;

use Insly\Recruiting\Models\AmountObject;
use Insly\Recruiting\Models\PriceObject;
use Insly\Recruiting\Interfaces\PriceAddition\iCustomPriceInitializer;


/**
 * Commission is added to base price (17%)
 *
 * if we have Base Price Policy added as addition custom price
 */
class CommissionPriceAddition
    extends aInslyPriceAddition
    implements iCustomPriceInitializer
{
    const TYPE = 'commission';

    /** @var PriceObject */
    protected $price;


    /**
     * Title
     *
     * @return string
     */
    function getTitle()
    {
        return 'Commission (17%)';
    }

    /**
     * Get Amount For an Specific Instalment Number
     *
     * @param int $n
     *
     * @return AmountObject
     */
    function calcAmountOnInstalment($n)
    {
        $n = (int) $n;
        if ($n <=0 )
            throw new \InvalidArgumentException('Instalment can`t be less than one.');


        if ( isset($this->_amount_on_instalment[$n]) )
            // we have it, don`t calculate again
            return $this->_amount_on_instalment[$n];


        if (! $this->price->hasCustomByType('base_premium') )
            // Do nothing!
            return new AmountObject;


        // 17% of base price
        $ac = $this->price->getAdditionCustomByType('base_premium');

        // We can add extra instalment value here if value of percentage depends on it
        // in exp. Step wise Pricing mechanism.
        $amount = new AmountObject;
        $prcent = (17 * $ac->getAmount()->getValue() / 100) / $this->instalment;
        $amount->setValue($prcent);

        $this->_amount_on_instalment[$n] = $amount;
        return $amount;
    }


    // Implement iCustomPriceInitializer:

    /**
     * Initialize Price Object
     *
     * @param PriceObject $price
     *
     * @return $this
     */
    function setPrice(PriceObject $price)
    {
        $this->price = $price;
        return $this;
    }
}
