<?php
namespace Insly\Recruiting\Exceptions;


class exRouteNotMatch
    extends \RuntimeException
{
    protected $code = 404;
}
