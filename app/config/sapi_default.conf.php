<?php
/**
 * Default Sapi Application Options
 */
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use Poirot\Router\Interfaces\iRouterStack;

use Insly\Recruiting\Services\EventsService;


return [

    'serviceLocator' => [
        ## Define The Concrete Implementations
        #
        'implementations' => [
            'HttpPsrRequest'  => RequestInterface::class,
            'HttpPsrResponse' => ResponseInterface::class,
            'Router'          => iRouterStack::class,

        ],

        ## Define Services
        #
        'services' => [
            'HttpPsrRequest'  => \Insly\Recruiting\Services\HttpRequestService::class,
            'HttpPsrResponse' => \Insly\Recruiting\Services\HttpResponseService::class,

            'Router'          => \Insly\Recruiting\Services\RouterService::class,

            'Events'          => new \Poirot\Ioc\instance( EventsService::class ),
        ],


        ## Nested New Namespace Services
        #
        #  - nested containers allow to access dependencies follow up to root container
        #
        #    IoC with nested implementation is my own solution to some containers problem like one we have
        #    inside Zend framework named Shared Container
        #
        'nested' => [
            'helpers' => [
                'services' => [
                    // Separate Insurance Calculation Logic
                    //
                    'CalculateCarInsurance' => \Insly\Recruiting\ActionsHelper\CalculateCarInsuranceService::class,
                ],
            ],
        ],
    ],
];
