<?php
/**
 * I have used some packages from my own framework named Poirot to Implement functionality
 */
use Poirot\Router\Route;

use \Insly\Recruiting as Insly;

include_once __DIR__.'/.index.pre.php';


// Define Routes
//
$server
    ->handleRequest(
        new Route\RouteMethodSegment('insurance.calc.calculate', [
            'criteria' => '/insurance/calc',
            'method'   => 'POST',
        ])
        , Insly\Actions\CalculateCarInsurancePage::class    // Handler
    )
    ->handleRequest(
        new Route\RouteMethodSegment('insurance.calc.calculate', [
            'criteria' => '/insurance/calc',
            'method'   => 'GET',
        ])
        , Insly\Actions\CarInsurancePage::class             // Handler
    )
    // Home, Task #1
    ->handleRequest(
        new Route\RouteSegment('home', [
            'criteria' => '/',
        ])
        , Insly\Actions\InslyNamePage::class                // Handler
    )
;

## Listen To Requests and Dispatch
#
$server->run();
