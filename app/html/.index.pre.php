<?php
/**
 * I have used some packages from my own framework named Poirot to Implement functionality
 */
use \Insly\Recruiting as Insly;


## Initializing Packages and Container Preparation -----------------------------------\
#
if (! file_exists(__DIR__.'/../vendor/autoload.php') )
    throw new \RuntimeException( "Unable to load Application.\n"
        . "- Type `composer install`; to install library dependencies.\n"
    );

if (! file_exists('/docker/initialized') )
    throw new \RuntimeException( "Initializing .....\n");


## Autoload Dependencies -------------------------------------------------------------\
#
require __DIR__.'/../vendor/autoload.php';

define('INSLY_DIR_ROOT', dirname(__FILE__));


## Load Application ------------------------------------------------------------------\
#
// change cwd to the application root by default
chdir( INSLY_DIR_ROOT );


// Build Server By Loading Dependencies From Config File
//
$config = Insly\Config\ArrayLoader::load(__DIR__.'/../config/sapi_default');
$server = new Insly\Server(
    new Insly\Server\BuildServer($config)
);

// Made services accessible from anywhere
IOC::GiveIoC( $server->services() );


## User Timezone Is Important -------------------------------------------------------------\
#
// TODO As we know PHP only displays server side time.
//      It's a bit tricky to get user local time here is a dirty fix for it.

$default = date_default_timezone_get();


// On many systems (Mac, for instance) "/etc/localtime" is a symlink
// to the file with the timezone info
if ( is_link("/etc/localtime") ) {

    // If it is, that file's name is actually the "Olsen" format timezone
    $filename = readlink("/etc/localtime");

    $pos = strpos($filename, "zoneinfo");
    if ($pos) {
        // When it is, it's in the "/usr/share/zoneinfo/" folder
        $timezone = substr($filename, $pos + strlen("zoneinfo/"));
    } else {
        // If not, bail
        $timezone = $default;
    }
}
else {
    // On other systems, like Ubuntu, there's file with the Olsen time
    // right inside it.
    $timezone = file_get_contents("/etc/timezone");
    if (!strlen($timezone)) {
        $timezone = $default;
    }
}


date_default_timezone_set($timezone);